    define(
        [
            'jquery',
            'ko',
            'uiComponent',
            'Magento_Customer/js/model/customer',
        ],
        function ($,ko, Component,customer ) {
            "use strict";

            var isCustomer = customer.isLoggedIn();

            if(isCustomer){
                if(window.customerData.extension_attributes.is_subscribed == false){
                    return Component.extend({
                        defaults: {
                            template: 'Radley_Newsletter/newsletter',
                            privacyRedirect: function () {
                                let data = {};
                                data.action = "relative.redirect";
                                data.target = "privacy-cookie-policy";
                                window.parent.postMessage(data, '*');
                                return false;
                            }
                        }
                    })
                }else{
                    return Component.extend({});
                }
            }else{
                return Component.extend({
                    defaults: {
                        template: 'Radley_Newsletter/newsletter',
                        privacyRedirect: function () {
                            let data = {};
                            data.action = "relative.redirect";
                            data.target = "privacy-cookie-policy";
                            window.parent.postMessage(data, '*');
                            return false;
                        },
                    }
                })
            }

        }

    );



