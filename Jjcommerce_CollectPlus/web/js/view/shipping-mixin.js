define(function () {
    'use strict';

    var mixin = {
        isConnectedCollectPlus: false,

        initObservable: function () {
            this._super().observe('isConnectedCollectPlus');
            return this;
        },
    };

    return function (Shipping) {
        return Shipping.extend(mixin);
    };
});
