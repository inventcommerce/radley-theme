/**
 * CollectPlus
 *
 * @category    CollectPlus
 * @package     Jjcommerce_CollectPlus
 * @version     2.0.0
 * @author      Jjcommerce Team
 *
 */
define([
    'jquery',
    'uiComponent',
    'ko',
    'Magento_Shipping/js/model/config',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Ui/js/lib/knockout/extender/bound-nodes',
    'Magento_Customer/js/model/customer',
    'uiRegistry'
], function ($, Component, ko, config, quote, shippingService, boundNodes, customer, registry) {
    'use strict';

    var collectavailable = Number(window.collectavailable);
    var collectlogo = window.collect_logo;
    var collectprice = window.checkoutConfig.collectplus_price;
    if(!collectavailable) {
        return Component.extend({
            config: config(),
            collectlogo:collectlogo,
            collectavailable:collectavailable,
            collectprice:collectprice
        });
    }
    return Component.extend({
        defaults: {
            template: 'Jjcommerce_CollectPlus/checkout/shipping/collectplus-select'
        },
        config: config(),
        collectlogo:collectlogo,
        collectavailable:collectavailable,
        collectprice:collectprice,
        quote: quote,
        checked: ko.observable(false),
        rates: shippingService.getShippingRates(),
        collectSimpleMethod: null,
        currentState: false,
        options: {
            shippingSectionRegistrySelector: 'index = shippingAddress',
            addressListRegistrySelector: 'index = address-list',
        },

        initEvents: function () {
            let self = this;
            $(".collectplus-select input.radio").on('uncheck', function () {
                self.checked(false);
            });
        },

        initialize: function () {
            let self = this;
            this._super();
            this.currentState = false;

            /** Set empty collect method from list */
            this.rates.subscribe(function (items) {
                if (items) {
                    $.each(items, function (i, item) {
                        if (item.method_code == 'collectsimple') {
                            self.collectSimpleMethod = item;
                            return false;
                        }
                    })
                }
            });

            quote.shippingMethod.subscribe(this.onShippingMethodChange, this);

            /** By selecting collect+ select empty method for right trigering delivety and address view */
            this.checked.subscribe(this.formAction, this);
        },

        formAction: function(val) {
            let self = this;

            if (!val) {
                return;
            }

            if (self.collectSimpleMethod) {
                quote.shippingMethod(self.collectSimpleMethod);
            }

            var city = $('div[name="shippingAddress.city"]');
            var postcode = $('div[name="shippingAddress.postcode"]');
            var region_id = $('div[name="shippingAddress.region_id"]');
            var country_id = $('div[name="shippingAddress.country_id"]');
            var street1 = $('div[name="shippingAddress.street.0"]');
            var street2 = $('div[name="shippingAddress.street.1"]');
            var street3 = $('div[name="shippingAddress.street.2"]');

            city.hide().find('input').val(null).trigger('change')
            postcode.hide().find('input').val(null).trigger('change')
            region_id.hide().find('select').val(null).trigger('change')
            country_id.hide().find('input').val(null).trigger('change')
            street1.hide().find('input').val(null).trigger('change')
            street2.hide().find('input').val(null).trigger('change')
            street3.hide().find('input').val(null).trigger('change')
            $('.hide-on-collect-plus').hide();
            window.resetshippingform = 1
        },

        toggleShippingAddressForm: function (addressFormSection) {
            var shippingListNodes,
                state = this.currentState;

            try {
                if (ko.isWriteableObservable(addressFormSection.isConnectedCollectPlus)) {
                    addressFormSection.isConnectedCollectPlus(state);
                }
            } catch (e) {
                console.error(e);
            }
        },

        toggleShippingAddressList: function (addressListSection) {
            var shippingListNodes,
                state = !this.currentState;

            try {
                if (ko.isWriteableObservable(addressListSection.visible)) {
                    addressListSection.visible(state);
                }
            } catch (e) {
                console.error(e);
            }
        },

        onShippingMethodChange: function (method) {
            this.currentState = method
                && (method['carrier_code'] === 'collectsimple' || method['carrier_code'] === 'collect');

            if (method['carrier_code'] !== 'amstorepickup' && customer.isLoggedIn()) {
                registry.get(this.options.shippingSectionRegistrySelector, this.toggleShippingAddressForm.bind(this));
                registry.get(this.options.addressListRegistrySelector, this.toggleShippingAddressList.bind(this));
            }
        }
    });

});
