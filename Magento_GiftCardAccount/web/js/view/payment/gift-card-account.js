/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_GiftCardAccount/js/action/set-gift-card-information',
    'Magento_GiftCardAccount/js/action/get-gift-card-information',
    'Magento_Checkout/js/model/totals',
    'Magento_GiftCardAccount/js/model/gift-card',
    'Magento_Checkout/js/model/quote',
    'Magento_Catalog/js/price-utils',
    'mage/validation',
    'mage/translate',
    'Magento_GiftCardAccount/js/model/payment/gift-card-messages'
], function ($, ko, Component, setGiftCardAction, getGiftCardAction, totals, giftCardAccount, quote, priceUtils, v, $t, messageList) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_GiftCardAccount/payment/gift-card-account',
            giftCartCode: ''
        },
        isLoading: getGiftCardAction.isLoading,
        giftCardAccount: giftCardAccount,

        /** @inheritdoc */
        initObservable: function () {
            this._super()
                .observe('giftCartCode');

            return this;
        },

        /**
         * Set gift card.
         */
        setGiftCard: function () {
            if (this.validate()) {
                setGiftCardAction([this.giftCartCode()]);
            }
        },

        /**
         * Check balance.
         */
        checkBalance: function () {
            if (this.validate()) {
                getGiftCardAction.check(this.giftCartCode());
            }
        },

        /**
         * @param {*} price
         * @return {String|*}
         */
        getAmount: function (price) {
            return priceUtils.formatPrice(price, quote.getPriceFormat());
        },

        /**
         * @return {jQuery}
         */
        validate: function () {
            //remove old card informations
            $('div.gift-card-information').remove();

            var form = '#giftcard-form';

            var giftCardCode = $(form).find('#giftcard-code').val()

            if (giftCardCode.indexOf(' ') >= 0) {
                messageList.addErrorMessage({message: $t('We\'re sorry, we cannot redeem this gift card! Please check your gift card number is correct and try again')})
                return false;
            }

            return $(form).validation() && $(form).validation('isValid');
        }
    });
});
