define([
    'Magento_Checkout/js/model/quote',
], function (quote) {
    'use strict';

    var mixin = {
        defaults: {
            isCurrentMethod: false
        },

        /** @inheritdoc */
        initialize: function () {
            this._super();

            let self = this;
            quote.shippingMethod.subscribe(function (val) {
                if (!val){
                    self.isCurrentMethod(false);
                }else{
                    self.isCurrentMethod(val.method_code == "flatrate");
                }
            });
        },

        /** @inheritdoc */
        initObservable: function () {
            this._super()
                .observe('isCurrentMethod');

            return this;
        },

        /**
         * Total rule for error showing
         * @returns {*}
         */
        totalRule: function () {
            return this.isCurrentMethod() && this.isVisible();
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
