define([
    'jquery',
    'uiRegistry',
], function ($, registry) {
    'use strict';

    var mixin = {
        defaults: {
            links: {
                collectChecked: 'checkout.steps.shipping-step.shippingAddress.before-shipping-method-form.collect_stores:checked'
            }
        },

        /** @inheritdoc */
        initObservable: function () {
            this._super()
                .observe('collectChecked');

            return this;
        },

        isVisibleWithCollect: function () {
            return this.isVisible() && !this.collectChecked();
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
