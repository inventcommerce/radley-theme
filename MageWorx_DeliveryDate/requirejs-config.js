var config = {
    config: {
        mixins: {
            'MageWorx_DeliveryDate/js/checkout/container': {
                'MageWorx_DeliveryDate/js/checkout/container-mixin': true
            },
            'MageWorx_DeliveryDate/js/checkout/error': {
                'MageWorx_DeliveryDate/js/checkout/error-mixin': true
            },
        }
    },
};
