define(['underscore','knockout'], function (_,ko) {
    'use strict';

    var mixin = {

        defaults: {
            blankval: ko.observable(true)
        },
        setInitialValue: function () {
            return (this.dataScope.indexOf('.prefix') != -1)
                ? this
                : this._super();
        },
        initObservable: function () {
            this._super();
            this.value.subscribe(this.valueIsEmptySubscriber,this)
            return this;
        },
        valueIsEmptySubscriber: function (value) {
            this.blankval(!value || value.trim().length === 0);
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
