/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    return function (placeOrderAction) {

        return wrapper.wrap(placeOrderAction, function (originalAction) {
            console.log('New Set shipping information');
            var isCollect = localStorage.getItem("collectplus");
            if(isCollect === 'yes') {
                var isTelephonequote = quote.shippingAddress().telephone;
                if (isTelephonequote === 'null' || isTelephonequote === '' || isTelephonequote === undefined) {
                    quote.shippingAddress().telephone = window.collectaddresstelephone;
                }
            }
            var shippingMethod = quote.shippingMethod();
            if (shippingMethod.hasOwnProperty('carrier_code')) {
                if (shippingMethod.carrier_code == 'amstorepickup'){
                    var registry   = require('uiRegistry');
                    var component  = registry.get('checkoutProvider');
                    var am = component.amstorepickup;
                    if (am.hasOwnProperty('first_name')) {
                        quote.shippingAddress().firstname = am.first_name;
                    }
                    if (am.hasOwnProperty('last_name')) {
                        quote.shippingAddress().lastname = am.last_name;
                    }
                    if (am.hasOwnProperty('telephone')) {
                        quote.shippingAddress().telephone = am.telephone;
                    }
                }
            }
            return originalAction();
        });
    };
});
