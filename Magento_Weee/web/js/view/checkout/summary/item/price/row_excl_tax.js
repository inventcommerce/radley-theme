/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */

define([
    'Magento_Weee/js/view/checkout/summary/item/price/weee'
], function (weee) {
    'use strict';

    return weee.extend({
        defaults: {
            template: 'Magento_Weee/checkout/summary/item/price/row_excl_tax'
        },

        /**
         * @param {Object} item
         * @return {Number}
         */
        getFinalRowDisplayPriceExclTax: function (item) {
            var rowTotalExclTax = parseFloat(item['row_total']);

            if (!window.checkoutConfig.getIncludeWeeeFlag) {
                rowTotalExclTax += parseFloat(item['weee_tax_applied_amount']);
            }

            return rowTotalExclTax;
        },

        /**
         * Function to test if to show orginal price
         *
         * @param currentPrice
         * @param originalPrice
         */
        showOriginalPrice: function (context1, context2) {
            var originalPrice = context1.getItemDetail(context2.item_id, 'was_price');
            var qty = context2.qty
            var originalTotalPrice = this.getRowTotalOriginalPrice(context1, context2)
            var currentTotalPrice  = this.getRowDisplayPriceExclTax(context2);
            if (currentTotalPrice !== originalTotalPrice) {
                return true;
            }
            return false;
        },

        /**
         * get original price * qty
         *
         * @param context1
         * @param context2
         * @returns {number}
         */
        getRowTotalOriginalPrice: function (context1, context2) {
            var originalPrice = context1.getItemDetail(context2.item_id, 'was_price');
            var qty = context2.qty
            var originalTotalPrice = Math.round(originalPrice * qty * window.checkoutConfig.currencyRate * 100) / 100;
            return originalTotalPrice;
        },

        /**
         * @param {Object} item
         * @return {Number}
         */
        getRowDisplayPriceExclTax: function (item) {
            var rowTotalExclTax = parseFloat(item['row_total']);

            if (window.checkoutConfig.getIncludeWeeeFlag) {
                rowTotalExclTax += this.getRowWeeeTaxExclTax(item);
            }

            return rowTotalExclTax;
        },

        /**
         * @param {Object} item
         * @return {Number}
         */
        getRowWeeeTaxExclTax: function (item) {
            var totalWeeeTaxExclTaxApplied = 0,
                weeeTaxAppliedAmounts;

            if (item['weee_tax_applied']) {
                weeeTaxAppliedAmounts = JSON.parse(item['weee_tax_applied']);
                weeeTaxAppliedAmounts.forEach(function (weeeTaxAppliedAmount) {
                    totalWeeeTaxExclTaxApplied += parseFloat(Math.max(weeeTaxAppliedAmount['row_amount'], 0));
                });
            }

            return totalWeeeTaxExclTaxApplied;
        }

    });
});
