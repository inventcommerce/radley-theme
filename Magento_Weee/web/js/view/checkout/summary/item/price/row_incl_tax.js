define([
    'Magento_Weee/js/view/checkout/summary/item/price/weee'
], function (weee) {
    'use strict';

    return weee.extend({
        defaults: {
            template: 'Magento_Weee/checkout/summary/item/price/row_incl_tax',
            displayArea: 'row_incl_tax'
        },

        /**
         * @param {Object} item
         * @return {Number}
         */
        getFinalRowDisplayPriceInclTax: function (item) {
            var rowTotalInclTax = parseFloat(item['row_total_incl_tax']);

            if (!window.checkoutConfig.getIncludeWeeeFlag) {
                rowTotalInclTax += this.getRowWeeeTaxInclTax(item);
            }

            return rowTotalInclTax;
        },

        /**
         * @param {Object} item
         * @return {Number}
         */
        getRowDisplayPriceInclTax: function (item) {
            var rowTotalInclTax = parseFloat(item['row_total_incl_tax']);

            if (window.checkoutConfig.getIncludeWeeeFlag) {
                rowTotalInclTax += this.getRowWeeeTaxInclTax(item);
            }

            return rowTotalInclTax;
        },

        /**
         * @param {Object}item
         * @return {Number}
         */
        getRowWeeeTaxInclTax: function (item) {
            var totalWeeeTaxInclTaxApplied = 0,
                weeeTaxAppliedAmounts;

            if (item['weee_tax_applied']) {
                weeeTaxAppliedAmounts = JSON.parse(item['weee_tax_applied']);
                weeeTaxAppliedAmounts.forEach(function (weeeTaxAppliedAmount) {
                    totalWeeeTaxInclTaxApplied += parseFloat(Math.max(weeeTaxAppliedAmount['row_amount_incl_tax'], 0));
                });
            }

            return totalWeeeTaxInclTaxApplied;
        },

        /**
         * get original price * qty
         *
         * @param context1
         * @param context2
         * @returns {number}
         */
        getRowTotalOriginalPrice: function (context1, context2) {
            let originalPrice = context1.getItemDetail(context2.item_id, 'was_price');
            let qty = context2.qty;
            return Math.round(originalPrice * qty * window.checkoutConfig.currencyRate * 100) / 100;
        },

        /**
         * Function to test if to show original price
         *
         * @param context1
         * @param context2
         */
        showOriginalPrice: function (context1, context2) {
            let originalPrice = context1.getItemDetail(context2.item_id, 'was_price');
            let qty = context2.qty;
            let originalTotalPrice = this.getRowTotalOriginalPrice(context1, context2);
            let currentTotalPrice  = this.getRowDisplayPriceInclTax(context2);
            return currentTotalPrice !== originalTotalPrice;
        }

    });
});
