define([
    'underscore',
    'ko',
    'Magento_Ui/js/form/element/abstract',
    'Amasty_StorePickupWithLocator/js/model/pickup/pickup-data-resolver',
    'Magento_Ui/js/model/messageList',
    'jquery'
], function (
    _,
    ko,
    Element,
    pickupDataResolver,
    globalMessageList,
    $
) {
    'use strict';

    var mixin = {
        initialize: function () {
            this._super();

            this.required(true);
            this.validation['required-entry'] = true;
            $(document).on('click', '#shipping-method-buttons-container', function(){
                this.validate();
                $('.messages.checkout-messages').click();
            }.bind(this));
            this.value.subscribe(_.throttle(this.onDataChange.bind(this), 500));

            return this;
        },

        onDataChange: function () {
            $('.messages.checkout-messages').click();
        },

        onChangeStore: function (storeId) {
            globalMessageList.clear();
            pickupDataResolver.storeId(storeId);
        },
    };

    return function (target) {
        return target.extend(mixin);
    };
});
