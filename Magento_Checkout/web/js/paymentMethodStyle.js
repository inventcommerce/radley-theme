/**
 *  Dynamically set the style for payment methods
 */
define([
    'jquery'
], function ($) {
    "use strict";

    function applyStyle() {
        var stepTitle = $('li.checkout-payment-method .step-content form.form.payments fieldset h2.step-title');
        var checkoutAg = $('li.checkout-payment-method .step-content form.form.payments fieldset.fieldset .payment-methods .payment-method._active .payment-method-content .checkout-agreements-block');
        var actionButton = $('li.checkout-payment-method .step-content form.form.payments fieldset.fieldset .payment-methods .payment-method .payment-method-content .actions-toolbar');
        var adyenCc = $('#adyen-cc-content');
        var klarnaDiv = $('#klarna-pay-later-container').parent('div');
        var klarnaDiv2 = $('#klarna-pay-over-time-container').parent('div');
        var klarnaDiv3 = $('#payment_fieldset_adyen_hpp_klarna_account');
        var stepTitlePosition = stepTitle.offset();
        if (stepTitlePosition) {
            adyenCc.css({"top": stepTitlePosition.top + stepTitle.outerHeight() + parseInt(stepTitle.css('marginBottom'))});
            checkoutAg.css({"top": stepTitlePosition.top + stepTitle.outerHeight() + parseInt(stepTitle.css('marginBottom'))});
            var checkoutAGPosition = checkoutAg.offset();
            if (checkoutAGPosition) {
                actionButton.css({"top": checkoutAGPosition.top + checkoutAg.outerHeight(true)})
            }

            if (klarnaDiv) {
                klarnaDivStyle(klarnaDiv)
            }

            if (klarnaDiv2) {
                klarnaDivStyle(klarnaDiv2)
            }

            if (klarnaDiv3) {
                klarnaDivStyle(klarnaDiv3)
            }
        }
    }

    function klarnaDivStyle(klarnaDiv) {
        let stepTitle = $('li.checkout-payment-method .step-content form.form.payments fieldset h2.step-title');
        let stepTitlePosition = stepTitle.offset();
        let windowsize = $(window).width();
        if (windowsize < 1166) {
            klarnaDiv.css({
                "position": "unset",
                "width": "100%",
                //RN-3845
                // "display": "inline-flex",
            });
        } else {
            klarnaDiv.css({
                "top": stepTitlePosition.top + stepTitle.outerHeight() + parseInt(stepTitle.css('marginBottom')),
                "position": "absolute",
                "left": "31%",
                "width": "30%",
                "display": "block",
            });
        }


        let checkoutAgKlarna = klarnaDiv.siblings('.checkout-agreements-block');
        let actionButtonKlarna = klarnaDiv.siblings('.actions-toolbar');
        let klarnaDivPosition = klarnaDiv.offset();
        if (klarnaDivPosition) {
            checkoutAgKlarna.css({"top": klarnaDivPosition.top + klarnaDiv.outerHeight() + parseInt(klarnaDiv.css('marginBottom'))});

            let checkoutAGPositionK = checkoutAgKlarna.offset();
            if (checkoutAGPositionK) {
                actionButtonKlarna.css({"top": checkoutAGPositionK.top + checkoutAgKlarna.outerHeight(true)})
            }
        }
    }

    /** apply style on different events*/
    $(window).resize(function() {
        applyStyle();
    });

    $('body').bind('DOMSubtreeModified click mousemove', function(){
        applyStyle();
    });

    $('body').bind('DOMSubtreeModified', function() {
        var loginForm = $('form[data-role=email-with-possible-login]');
        if(loginForm[0] && loginForm[0].hasAttribute("style")) {
            loginForm.removeAttr("style");
        }
    });

    $('body').bind('mousemove', function(){
        fixTranslate()
    });

    $(window).resize(function(){
        fixTranslate()
    });

    function fixTranslate() {
        const element = document.querySelector("div.payment-method-title.field.choice");
        if (element === null) {
            return;
        }
        var ratio = parseFloat(window.devicePixelRatio);
        var valueX,valueY;

        if (ratio < 0.75) {
            valueX = valueY = 23
        } else if (0.75 <= ratio && ratio < 0.87) {
            valueX = valueY = 20
        } else if (0.87 <= ratio && ratio < 1.10) {
            valueX = valueY = 18
        } else if (1.10 <= ratio && ratio < 1.25) {
            valueX = 17
            valueY = 20
        } else if (ratio == 1.25) {
            valueX = valueY = 17
        } else if (ratio > 1.25) {
            valueX = valueY = 16
        }

        const translate = "translate(" + valueX + "%, " + valueY + "%)"
        if(element !== null)
        {
            element.style.setProperty("--translate-value", translate);
        }
    }
});
