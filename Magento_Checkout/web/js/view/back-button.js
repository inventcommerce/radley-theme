define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/step-navigator',
    'mage/translate',
    'Magento_Ui/js/model/messageList',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/select-billing-address',
    'Amasty_StorePickupWithLocator/js/model/pickup',
    'Amasty_StorePickupWithLocator/js/action/shipping-address-form',
    'Magento_Checkout/js/checkout-data'
], function (
    $,
    ko,
    Component,
    stepNavigator,
    $t,
    MessageList,
    quote,
    selectBillingAddress,
    pickup,
    shippingAddressFormActions,
    checkoutData
) {
    "use strict";

    /** Hides the collect+ div when selected country is not Great Britain **/
    var CollectToggle = function (countryId) {
        cityAndPostCodeToggle();
        if (countryId === '' || countryId === 'GB') {
            $('#collect-plus-div').show();
        } else {
            if(countryId!=undefined) {
                $('#collect-plus-div').hide();
            }
        }
    }

    $(document).on('click', '.billing-address-same-as-shipping-block', function(){
        var registry   = require('uiRegistry');
        var component  = registry.get('checkoutProvider');
        var addr       = component.shippingAddress;
        var shippAddr = quote.shippingAddress();
        var map = {
            'city'       : 'city',
            'company'    : 'company',
            'country_id' : 'countryId',
            'firstname'  : 'firstname',
            'lastname'   : 'lastname',
            'prefix'     : 'prefix',
            'region'     : 'region',
            'region_id'  : 'region_id',
            'street'     : 'street',
            'telephone'  : 'telephone',
        };
        var cityFound = false;
        for (var ii in map) {
            if(addr.hasOwnProperty(ii) && shippAddr.hasOwnProperty(map[ii])){
                var cur = map[ii];
                if(shippAddr[cur] !== undefined && shippAddr[cur] !== ''){
                    if(cur == 'city' && addr.city == ''){
                        cityFound = true;
                    }
                    if(cityFound){
                        addr[ii] = shippAddr[cur];
                    }
                }
            }
        }
    });

    /**
     * toggle for postcode & telephone fields
     * for specific options
     */
    var cityAndPostCodeToggle = function (){
        var ifEventExists = function (){
            if(event != undefined &&
                event.currentTarget != undefined &&
                event.currentTarget.id != undefined){
               return true;
            }
            return false;
        }
        var ifSrcEventExists = function (){
            if(event.srcElement != undefined &&
                event.srcElement.id != undefined &&
                event.srcElement.id != ''){
                return true;
            }
            return false;
        }
        if (ifEventExists()) {
            var city       = $('div[name="shippingAddress.city"]');
            var postcode   = $('div[name="shippingAddress.postcode"]');
            var tel        = $('div[name="shippingAddress.telephone"]');
            var enabledFor = 's_method_amstorepickup_amstorepickup';
            var curId      = event.currentTarget.id;
            if(curId === undefined || curId == ''){
                if(ifSrcEventExists()){
                    var item = $('.'+event.srcElement.id).find('[name="shipping_method"]');
                    if(item.length){
                        curId = $('.'+event.srcElement.id).find('[name="shipping_method"]').attr('id');
                    }
                }
            }
            if(curId.startsWith('s_method') == false){
                tel.find('.field-error').show();
                return;
            }
            if(curId != enabledFor && curId != undefined && curId != ''){
                city.hide(); postcode.hide();
                tel.removeClass('_error');tel.find('.field-error').hide();
            } else {
                city.show(); postcode.show();
            }
        }
    }

    $(document).on('click', "#as_a_guest", function () {
        jQuery('html, body').animate({scrollTop:jQuery('#shipping').position().top }, 'fast');

    });

    quote.shippingAddress.subscribe(function (value) {
        CollectToggle(value.countryId);
    });

    $(document).on('change', "[name='country_id']", function () {
        var countryId = $('select[name="country_id"]:visible').val();
        CollectToggle(countryId);
    });

    $(document).on('click', ".afd-manual-input-button", function () {
        $("div[name$='.company']" ).hide();
        $('.afd-typeahead-error').hide();
    });

    $(document).on('click', '.afd-manual-input-search-button', function(){
        $('div[name="shippingAddress.city"]').hide();
        $('div[name="shippingAddress.postcode"]').hide();
        $('.afd-typeahead-error').show();
    });

    $(document).on('click', '.afd-typeahead-item.afd-typeahead-group-lookup', function(){
        var curr = $('select[name="country_id"]').val();
        var cnt = 1;
        var updatePost = setInterval(function() {
            cnt++; if(cnt > 5){clearInterval(updatePost); return;}
            if (($('div[name="shippingAddress.street.0"]').find('input').val() != '')
                && ($('div[name="shippingAddress.city"]').find('input').val() != '')
                && $('div[name="shippingAddress.postcode"]').find('input').val() == '') {
                $('div[name="shippingAddress.postcode"]').show();
            }
        }, 200, cnt);
    });

    $(document).on('click', "[name='billing[use_for_shipping]']", function () {
        if ($(this).prop('checked') === true) {
            if(pickup.isPickup()){
                try {
                    pickup.isPickup = false;
                    shippingAddressFormActions.toggleShippingAddressForm();
                    shippingAddressFormActions.toggleShippingSection();
                    shippingAddressFormActions.toggleShippingAddressList();
                } catch (e) {
                    console.error(e);
                }
            }
            localStorage.setItem("collectplus", "yes");
        } else {
            localStorage.setItem("collectplus", "no");
        }
    });

    $(document).on('click',".table-checkout-shipping-method", function() {
        var registry   = require('uiRegistry');
        registry.get('index = delivery_day').reset();
    });

    $(document).on('click',".table-checkout-shipping-method input.radio:not(#s_method_collect_collect_next)", function() {
        if ( $(this).prop('checked') === true) {
            $(".collectplus-select input.radio").prop('checked', false);
            $("#findyournearestbutton").hide();
            $('div[name="shippingAddress.telephone"]').show();
            $('div[name="shippingAddress.country_id"]').show();
            $('.hide-on-collect-plus').show();
        }
    });

    $(document).on('click', '#collectplus_popup_action_button_save', function(){
        let phoneNumber = $('input[name="sms_alert"]').val();
        if (phoneNumber.length){
            $('div[name="shippingAddress.telephone"]').show().val(phoneNumber);
        } else {
            $('div[name="shippingAddress.telephone"]').show();
        }
    });

    $(document).on('click', '#collectplus_popup_action_button_no_thanks', function(){
        $('div[name="shippingAddress.telephone"]').show();
    });

    $(document).on('click', '#collectplus_popup_close-sms', function(){
        $('div[name="shippingAddress.telephone"]').show();
    });

    $(document).on('change', '#customer-password', function(){
        $('#checkout-login-error').remove();
        $('#customer-password').removeClass('mage-error').addClass('valid');
    });

    $(document).on('click', ".mobile-title", function () {
        if($(window).width() <= 767){
            if( jQuery('#payment').css('padding-top') == '0px'){
                jQuery('#payment').css('padding-top', jQuery('.summary-content').height())
            } else {
                jQuery('#payment').css('padding-top', '0px');
            }
        }
    });

    $(document).on('click', ".action.action-show-popup-new-address", function () {
        $(document).on('click', '.action.primary.action-save-address', function(){
            if($('div[name="shippingAddress.street.0"]').is(":hidden")){
                $('.afd-manual-input-button').click();
            }
        });
    });

    $(document).on('click', '#checkout-shipping-method-load .col.col-method.radio-button._with-tooltip', function () {
        var radio = $(this).find('input[type=radio]')

        if (window.collectaddressused == 1
            || window.resetshippingform !== undefined
        ) {
            if (radio.val().includes('amstrates')
                || radio.val().includes('flatrate')
                || radio.val().includes('freeshipping')
            ) {
                delete window.resetshippingform
                ResetShippingForm()
                clearCollectStore()
            }
        }

    });

    var ResetShippingForm = function () {
        var typeahed = $('.afd-typeahead-container');
        var city = $('div[name="shippingAddress.city"]');
        var postcode = $('div[name="shippingAddress.postcode"]');
        var telephone = $('div[name="shippingAddress.telephone"]');
        var region_id = $('div[name="shippingAddress.region_id"]');
        var country_id = $('div[name="shippingAddress.country_id"]');
        var street1 = $('div[name="shippingAddress.street.0"]');
        var street2 = $('div[name="shippingAddress.street.1"]');
        var street3 = $('div[name="shippingAddress.street.2"]');

        city.hide().find('input').prop('readonly', false);
        postcode.hide().find('input').prop('readonly', false);
        street1.hide().find('input').prop('readonly', false);
        street2.hide().find('input').prop('readonly', false);
        street3.hide().find('input').prop('readonly', false);
        telephone.show().find('input').prop('readonly', false);
        region_id.show();
        country_id.show();
        typeahed.show();
        $('.afd-search-again').trigger('click')
        $('.afd-manual-input-search-button').trigger('click')
        $('#nearestbuttontext').html('Find your nearest Collect+ store');
    }

    $(document).on('keyup', '#giftcard-code', function(){
        var giftCardCode = $(this).val()

        if (giftCardCode.indexOf(' ') === -1) {
            $('span.gift-error > div > div.message-error').remove()
        }
    });

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/back-button'
        },

        initialize: function () {
            this._super();
            pickup.isPickup.subscribe(this.pickupStateObserver, this);
            return this;
        },

        /**
         * @param {Boolean} isActive
         * @returns {void}
         */
        pickupStateObserver: function (isActive) {
            if (isActive) {
                ResetShippingForm();
                clearCollectStore();
            } else {
                window.resetshippingform = 1
            }
        },

        goToPrevStep: function () {
            stepNavigator.navigateTo('shipping');
        }
    })
});
