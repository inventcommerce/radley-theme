/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'underscore',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/step-navigator'
], function ($, _, ko, Component, stepNavigator) {
    'use strict';

    var steps = stepNavigator.steps;

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/progress-bar',
            visible: true
        },
        steps: steps,

        /** @inheritdoc */
        initialize: function () {
            var stepsValue;

            this._super();
            window.addEventListener('hashchange', _.bind(stepNavigator.handleHash, stepNavigator));

            if (!window.location.hash) {
                stepsValue = stepNavigator.steps();

                if (stepsValue.length) {
                    stepNavigator.setHash(stepsValue.sort(stepNavigator.sortItems)[0].code);
                }
            }

            stepNavigator.handleHash();
        },

        /**
         * @param {*} itemOne
         * @param {*} itemTwo
         * @return {*|Number}
         */
        sortItems: function (itemOne, itemTwo) {
            return stepNavigator.sortItems(itemOne, itemTwo);
        },

        /**
         * @param {Object} step
         */
        navigateTo: function (step) {
            stepNavigator.navigateTo(step.code);
        },

        /**
         * @param {Object} item
         * @return {*|Boolean}
         */
        isProcessed: function (item) {
            var pref = [
                'shippingAddress.prefix',
                'billingAddressadyen_cc.prefix',
                'billingAddressklarna_pay_over_time.prefix',
                'billingAddressklarna_pay_later.prefix',
                'billingAddresscashondelivery.prefix'
            ];
            for (var i in pref){
           //     this.updatePrefixes(pref[i]);
            }

         var itemCode = item.code;
            if (itemCode == "shipping") {
                jQuery('body').addClass('payment-step');
                jQuery('body').removeClass('shipping-step');
            } else {
                jQuery('body').addClass('shipping-step');
                jQuery('body').removeClass('payment-step');
            }
            return stepNavigator.isProcessed(item.code);
        },

        /**
         * Title palceholder toggle billing
         * @type {number}
         */
        updatePrefixes(xpathIn)
        {
            if (!xpathIn || xpathIn.indexOf('prefix') == -1) {
                return;
            }
            const xpathCheck = xpathIn+"";

            var cnt = 1;
            var checkExistBilling = setInterval(function() {
                cnt++; if(cnt > 100){clearInterval(checkExistBilling); return;}

                if (!xpathCheck || xpathCheck.indexOf('prefix') == -1) {
                    console.log('clear interval ',xpathCheck);
                    clearInterval(checkExistBilling); return;
                }


                var billAddPrefXpath   ='[name="'+xpathCheck+'"]';
                if ($(billAddPrefXpath).length) {
                    $(billAddPrefXpath).change(function() {

                        var initialName = xpathCheck;
                        if ($(this).find('select').children('option:first-child').is(':selected')) {
                            $('[name="'+xpathCheck+' change-prefix"]').attr('name',initialName);
                        } else {
                            $('[name="'+xpathCheck+'"]').attr('name',initialName + ' change-prefix');
                        }
                    });
                    clearInterval(checkExistBilling); return;
                }
            }, 200, cnt);
        }
    });
});
