/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
  'jquery',
  'underscore',
  'Magento_Ui/js/form/form',
  'ko',
  'Magento_Customer/js/model/customer',
  'Magento_Customer/js/model/address-list',
  'Magento_Checkout/js/model/address-converter',
  'Magento_Checkout/js/model/quote',
  'Magento_Checkout/js/action/create-shipping-address',
  'Magento_Checkout/js/action/select-shipping-address',
  'Magento_Checkout/js/model/shipping-rates-validator',
  'Magento_Checkout/js/model/shipping-address/form-popup-state',
  'Magento_Checkout/js/model/shipping-service',
  'Magento_Checkout/js/action/select-shipping-method',
  'Magento_Checkout/js/model/shipping-rate-registry',
  'Magento_Checkout/js/action/set-shipping-information',
  'Magento_Checkout/js/model/step-navigator',
  'Magento_Ui/js/modal/modal',
  'Magento_Checkout/js/model/checkout-data-resolver',
  'Magento_Checkout/js/checkout-data',
  'Magento_Ui/js/model/messageList',
  'uiRegistry',
  'mage/translate',
  'Magento_Checkout/js/model/shipping-rate-service',
], function (
  $,
  _,
  Component,
  ko,
  customer,
  addressList,
  addressConverter,
  quote,
  createShippingAddress,
  selectShippingAddress,
  shippingRatesValidator,
  formPopUpState,
  shippingService,
  selectShippingMethodAction,
  rateRegistry,
  setShippingInformationAction,
  stepNavigator,
  modal,
  checkoutDataResolver,
  checkoutData,
  globalMessageList,
  registry,
  $t
) {
  'use strict';

  var popUp = null;

  return Component.extend({
    defaults: {
      template: 'Magento_Checkout/shipping',
      shippingFormTemplate: 'Magento_Checkout/shipping-address/form',
      shippingMethodListTemplate: 'Magento_Checkout/shipping-address/shipping-method-list',
      shippingMethodItemTemplate: 'Magento_Checkout/shipping-address/shipping-method-item',
      links: {
        collectChecked: 'checkout.steps.shipping-step.shippingAddress.before-shipping-method-form.collect_stores:checked'
      }
    },
    visible: ko.observable(!quote.isVirtual()),
    errorValidationMessage: ko.observable(false),
    isCustomerLoggedIn: customer.isLoggedIn,
    isFormPopUpVisible: formPopUpState.isVisible,
    isFormInline: addressList().length === 0,
    isNewAddressAdded: ko.observable(false),
    saveInAddressBook: 1,
    quoteIsVirtual: quote.isVirtual(),
    enableNextButton: ko.observable(false),
    enableShippingButton: ko.observable(true),

    /**
     * @return {exports}
     */
    initialize: function () {
      var self = this,
        hasNewAddress,
        fieldsetName = 'checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset';

      this._super();

      if (!quote.isVirtual()) {
        stepNavigator.registerStep(
          'shipping',
          '',
          $t('Shipping'),
          this.visible, _.bind(this.navigate, this),
          this.sortOrder
        );
      }
      checkoutDataResolver.resolveShippingAddress();

      hasNewAddress = addressList.some(function (address) {
        return address.getType() == 'new-customer-address'; //eslint-disable-line eqeqeq
      });

      this.isNewAddressAdded(hasNewAddress);

      this.isFormPopUpVisible.subscribe(function (value) {
        if (value) {
          self.getPopUp().openModal();
        }
      });

      quote.shippingMethod.subscribe(function () {
        globalMessageList.clear();
        self.errorValidationMessage(false);
      });

	/** Preselect subcollect method */
	this.rates.subscribe(function () {
    $(document).ready(function(){
        setTimeout(function () {
            for (var name in window.checkoutConfig.shipping_tooltips) {
                if($('.label_method_'+name+' .field-tooltip-content').length){
                    $('.label_method_'+name+' .field-tooltip-content').html('');
                    $('.label_method_'+name+' .field-tooltip-content').append(window.checkoutConfig.shipping_tooltips[name])
                }
                if($('.'+name+' .field-tooltip-content').length){
                    $('.'+name+' .field-tooltip-content').html('');
                    $('.'+name+' .field-tooltip-content').append(window.checkoutConfig.shipping_tooltips[name])
                }
            }
        }, 2000);
    });
	if (self.rates() && self.rates().length) {
	    $.each(self.rates(), function (index, method) {
		if (method.method_code.indexOf('collect_') !== -1) {
		    quote.shippingMethod(method);
		    return false;
		}
	    });
	}
	});

      registry.async('checkoutProvider')(function (checkoutProvider) {
        var isShipingStep = function () {
            let hash = window.location.hash.replace('#', ''),
              result = hash == "shipping";
            // console.log('isShipingStep', hash, result, stepNavigator.isProcessed('shipping'));
            return result;
          },
          fieldsIsInited = false,
          initFields = function () {
            // console.log('isShipingStep', 'initFields');
            shippingRatesValidator.initFields(fieldsetName);
            fieldsIsInited = true;
          };

        var shippingAddressData = checkoutData.getShippingAddressFromData();
        if (shippingAddressData && isShipingStep()) {
          self.enableNextButton(false);
          checkoutProvider.set(
            'shippingAddress',
            $.extend(true, {}, checkoutProvider.get('shippingAddress'), shippingAddressData)
          );
          self.enableNextButtonCheck();
        }
        checkoutProvider.on('shippingAddress', function (shippingAddrsData) {
          if (isShipingStep()) {
            if (!fieldsIsInited) {
              initFields();
            }
            checkoutData.setShippingAddressFromData(shippingAddrsData);
          }
        });
        if (isShipingStep()) {
          initFields();
        }
      });

      shippingService.isLoading.subscribe(function () {
        self.enableNextButtonCheck();
      });
      return this;
    },

    /** @inheritdoc */
    initObservable: function () {
      this._super()
      .observe('collectChecked');

      return this;
    },

    /**
     * Navigator change hash handler.
     *
     * @param {Object} step - navigation step
     */
    navigate: function (step) {
      step && step.isVisible(true);
    },

/**
 *
 * @param {Object} step - navigation step
 */
 collectRadioBehavior: function () {
    $(document).on('change',".collectplus-select input.radio", function() {
        if ( $(this).prop('checked') === true &&
            quote.shippingMethod().method_code.indexOf('collect_') === -1
        ) {
          $(".table-checkout-shipping-method input[name='shipping_method']").prop('checked', false);
          $('div[name="shippingAddress.telephone"]').hide();
          $('div[name="shippingAddress.country_id"]').hide();
          $('.hide-on-collect-plus').hide();
        }
      });
    },

    /**
     *
     * @param {Object} step - navigation step
     */
    hideCollect: function () {
      console.log('done sm');
      $(document).on('click', ".table-checkout-shipping-method input.radio", function () {
        if ($(this).prop('checked') === true) {
          $(".collectplus-select input.radio").prop('checked', false);
          $("#findyournearestbutton").hide();
        }
      });
    },

    /**
     * @return {*}
     */
    getPopUp: function () {
      var self = this,
        buttons;

      if (!popUp) {
        buttons = this.popUpForm.options.buttons;
        this.popUpForm.options.buttons = [
          {
            text: buttons.save.text ? buttons.save.text : $t('Save Address'),
            class: buttons.save.class ? buttons.save.class : 'action primary action-save-address',
            click: self.saveNewAddress.bind(self)
          },
          {
            text: buttons.cancel.text ? buttons.cancel.text : $t('Cancel'),
            class: buttons.cancel.class ? buttons.cancel.class : 'action secondary action-hide-popup',

            /** @inheritdoc */
            click: this.onClosePopUp.bind(this)
          }
        ];

        /** @inheritdoc */
        this.popUpForm.options.closed = function () {
          self.isFormPopUpVisible(false);
        };

        this.popUpForm.options.modalCloseBtnHandler = this.onClosePopUp.bind(this);
        this.popUpForm.options.keyEventHandlers = {
          escapeKey: this.onClosePopUp.bind(this)
        };

        /** @inheritdoc */
        this.popUpForm.options.opened = function () {
          // Store temporary address for revert action in case when user click cancel action
          self.temporaryAddress = $.extend(true, {}, checkoutData.getShippingAddressFromData());
        };
        popUp = modal(this.popUpForm.options, $(this.popUpForm.element));
      }

      return popUp;
    },

    /**
     * Revert address and close modal.
     */
    onClosePopUp: function () {
      checkoutData.setShippingAddressFromData($.extend(true, {}, this.temporaryAddress));
      this.getPopUp().closeModal();
    },

    /**
     * Show address form popup
     */
    showFormPopUp: function () {
      this.isFormPopUpVisible(true);
    },

    /**
     * Save new shipping address
     */
    saveNewAddress: function () {
      var addressData,
        newShippingAddress;

      this.source.set('params.invalid', false);
      this.triggerShippingDataValidateEvent();

      if (!this.source.get('params.invalid')) {
        addressData = this.source.get('shippingAddress');
        // if user clicked the checkbox, its value is true or false. Need to convert.
        addressData['save_in_address_book'] = this.saveInAddressBook ? 1 : 0;

        // New address must be selected as a shipping address
        newShippingAddress = createShippingAddress(addressData);
        selectShippingAddress(newShippingAddress);
        checkoutData.setSelectedShippingAddress(newShippingAddress.getKey());
        checkoutData.setNewCustomerShippingAddress($.extend(true, {}, addressData));
        this.getPopUp().closeModal();
        this.isNewAddressAdded(true);
      }
    },

    /**
     * Shipping Method View
     */
    rates: shippingService.getShippingRates(),
    isLoading: shippingService.isLoading,
    isSelected: ko.computed(function () {
      return quote.shippingMethod() ?
        quote.shippingMethod()['carrier_code'] + '_' + quote.shippingMethod()['method_code'] :
        null;
    }),

    /**
     * @param {Object} shippingMethod
     * @return {Boolean}
     */
    selectShippingMethod: function (shippingMethod) {
            if (!shippingMethod || (
                shippingMethod.method_code.indexOf('collect_') === -1
            )) {
                $(".collectplus-select input.radio").prop('checked', false).trigger('uncheck');
                $("#findyournearestbutton").hide();
                $('div[name="shippingAddress.telephone"]').show();
                $('div[name="shippingAddress.country_id"]').show();
                $('.hide-on-collect-plus').show();
            }
      selectShippingMethodAction(shippingMethod);
      checkoutData.setSelectedShippingRate(shippingMethod['carrier_code'] + '_' + shippingMethod['method_code']);

      return true;
    },

    /**
     * Set shipping information handler
     */
    setShippingInformation: function () {
        if (this.validateShippingInformation()
            && (quote.shippingMethod()['method_code'] !== 'collectsimple'
                || $("#shipping_agent_id")[0])
        ) {
        quote.billingAddress(null);
        checkoutDataResolver.resolveBillingAddress();
        registry.async('checkoutProvider')(function (checkoutProvider) {
          var shippingAddressData = checkoutData.getShippingAddressFromData();

          if (shippingAddressData) {
            checkoutProvider.set(
              'shippingAddress',
              $.extend(true, {}, checkoutProvider.get('shippingAddress'), shippingAddressData)
            );
          }
        });
        setShippingInformationAction().done(
          function () {
            stepNavigator.next();
          }
        );
      } else {
        var msg = 'Please enter your desired address for delivery to continue.';

        if (quote.shippingMethod().method_code.indexOf('collect') !== -1) {
          msg = 'Please select a collect+ address for delivery to continue.'
        } else {
          if($('div[name="shippingAddress.postcode"]').find('.field-error').length){
            $('div[name="shippingAddress.postcode"]').show();
            $('div[name="shippingAddress.city"]').show();
          }
        }

        this.errorValidationMessage(
          $t(msg)
        );
      }
      this.toggleAdyenAutoFillAddrError();
    },

    /**
     * toggle for error message for adyen addr select
     */
    toggleAdyenAutoFillAddrError: function () {
      var toAdd, toRem;
      var shipAddrOne = '[name="shippingAddress.street.0"]';
      var adyenAddrSearch = '.afd-typeahead-error';
      var afdClass = 'afd-error';
      var nativeClass = 'field-error';
      if ($(shipAddrOne).hasClass('_error')) {
        toAdd = nativeClass;
        toRem = afdClass;
      } else {
        toAdd = afdClass;
        toRem = nativeClass;
      }
      $(adyenAddrSearch).removeClass(toRem);
      $(adyenAddrSearch).addClass(toAdd);
    },

    /**
     * @return {Boolean}
     */
    validateShippingInformation: function () {
      var shippingAddress,
        addressData,
        loginFormSelector = 'form[data-role=email-with-possible-login]',
        emailValidationResult = customer.isLoggedIn(),
        field,
        country = registry.get(this.parentName + '.shippingAddress.shipping-address-fieldset.country_id'),
        countryIndexedOptions = country.indexedOptions,
        option = countryIndexedOptions[quote.shippingAddress().countryId],
        messageContainer = registry.get('checkout.errors').messageContainer;

      if (!quote.shippingMethod()) {
        this.errorValidationMessage(
          $t('The shipping method is missing. Select the shipping method and try again.')
        );

        return false;
      }

      if (!customer.isLoggedIn()) {
        $(loginFormSelector).validation();
        emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
      }

      if (this.isFormInline) {
        this.source.set('params.invalid', false);
        this.triggerShippingDataValidateEvent();

        if (emailValidationResult &&
          this.source.get('params.invalid') ||
          !quote.shippingMethod()['method_code'] ||
          !quote.shippingMethod()['carrier_code']
        ) {
          this.focusInvalid();

          return false;
        }

        shippingAddress = quote.shippingAddress();
        addressData = addressConverter.formAddressDataToQuoteAddress(
          this.source.get('shippingAddress')
        );

        //Copy form data to quote shipping address object
        for (field in addressData) {
          if (addressData.hasOwnProperty(field) &&  //eslint-disable-line max-depth
            shippingAddress.hasOwnProperty(field) &&
            typeof addressData[field] != 'function' &&
            _.isEqual(shippingAddress[field], addressData[field])
          ) {
            shippingAddress[field] = addressData[field];
          } else if (typeof addressData[field] != 'function' &&
            !_.isEqual(shippingAddress[field], addressData[field])) {
            shippingAddress = addressData;
            break;
          }
        }

        if (customer.isLoggedIn()) {
          shippingAddress['save_in_address_book'] = 1;
        }
        selectShippingAddress(shippingAddress);
      } else if (customer.isLoggedIn() &&
        option &&
        option['is_region_required'] &&
        !quote.shippingAddress().region
      ) {
        messageContainer.addErrorMessage({
          message: $t('Please specify a regionId in shipping address.')
        });

        return false;
      }

      if (!emailValidationResult) {
        $(loginFormSelector + ' input[name=username]').focus();

        return false;
      }

      return true;
    },

    /**
     * Trigger Shipping data Validate Event.
     */
    triggerShippingDataValidateEvent: function () {
      this.source.trigger('shippingAddress.data.validate');

      if (this.source.get('shippingAddress.custom_attributes')) {
        this.source.trigger('shippingAddress.custom_attributes.data.validate');
      }
    },

    /**
     * Enable CTA (Next) button or not
     * @returns {boolean}
     */
    enableNextButtonCheck: function () {
      var result = false;
      if (this.rates().length) {
        $.each(this.rates(), function (k, v) {
          if (v.available) {
            result = true;
            return false;
          }
        });
      }

      this.enableNextButton(result);
    },

    /** TODO: RN-2243 */
    enableShippingButtonCheck: function () {
      let addr = quote.shippingAddress();
      var result = typeof addr.street != "undefined"
        && (typeof addr.firstname != "undefined" && addr.firstname.length)
        && (typeof addr.lastname != "undefined" && addr.lastname.length)
        && typeof addr.city != "undefined"
        && typeof addr.telephone != "undefined"
        && typeof addr.countryId !== "undefined";
      this.enableShippingButton(result);
    },

    /**
     * Check if selected collect+ and doesnt selected child method
     * This rule to be reviewed "!this.collectChecked" always returns true
     */
    collectPlusRule: function () {
      return !this.collectChecked() || (
        quote.shippingMethod() &&
        quote.shippingMethod().method_code.indexOf('collect') !== -1
      );
    },

    /**
     * @returns {boolean}
     */
    isSelectedShippmentMethod: function () {
      return !!quote.shippingMethod();
    }
  });
});
