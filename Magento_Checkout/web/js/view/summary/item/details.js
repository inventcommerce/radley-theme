/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'jquery',
], function (Component, quote, $) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/summary/item/details'
        },

        /**
         * Function to get product details
         *
         * @param itemId
         * @param attribute
         * @returns {*}
         */
        getProductDetail(itemId, attribute) {
            var result;
            $.each(quote.getItems(), function (idx, val) {
                if (itemId == val.item_id) {
                    result = val['product'][attribute];
                    return false;
                }
            });

            return result
        },

        /**
         * Function to get item details
         *
         * @param itemId
         * @param attribute
         * @returns {*}
         */
        getItemDetail(itemId, attribute) {
            var result;
            $.each(quote.getItems(), function (idx, val) {
                if (itemId == val.item_id) {
                    result = val[attribute];
                    return false;
                }
            });

            return result
        },

        /**
         * @param {Object} quoteItem
         * @return {String}
         */
        getValue: function (quoteItem) {
            return quoteItem.name;
        }
    });
});
