define([
    'jquery',
    'jquery/ui',
    'jquery/validate',
    'mage/translate'
], function($){
    'use strict';
    return function() {
        $.validator.addMethod(
            "email-sign-rule",
            function(value) {
                let result = (typeof value != "undefined" && value && value.indexOf('@') !== -1);
                if (!result) {
                    this.errorMessage = $.mage
                        .__("We're sorry, your email %1 does not currently include an @. Please add this in to continue")
                        .replace('%1', value);
                }
                return result;
            },
            function () {
                return this.errorMessage;
            }
        );
        $.validator.addMethod(
            "radley-telephone-us",
            function(value) {
                var phoneNumber;
                var result = true;
                var errorMessage;
                if (value === null) {
                    result = false;
                }
                phoneNumber = $.trim(value);
                if(phoneNumber.length){
                    result = Boolean(phoneNumber.match(/^[+]*[0-9]{0,3}[\s]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\/0-9]{5,13}$/));
                    errorMessage = 'Oops, it looks like your mobile number is invalid. Please check your mobile number contains 6-16 and only contains numbers, +- spaces and ()';
                } else {
                    result = false;
                    errorMessage = 'This is a required field.';
                }
                if (!result) {
                    this.errorMessage = $.mage
                        .__(errorMessage)
                        .replace('%1', value);
                }
                return result;
            },
            function () {
                return this.errorMessage;
            }
        );
    }
});
