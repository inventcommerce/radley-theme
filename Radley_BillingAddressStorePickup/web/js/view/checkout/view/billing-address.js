/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'ko',
        'underscore',
        'Magento_Ui/js/form/form',
        'Magento_Customer/js/model/customer',
        'Magento_Customer/js/model/address-list',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/create-billing-address',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/action/set-billing-address',
        'Magento_Ui/js/model/messageList',
        'mage/translate',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'uiRegistry',
    ],
    function (
        ko,
        _,
        Component,
        customer,
        addressList,
        quote,
        createBillingAddress,
        selectBillingAddress,
        checkoutData,
        checkoutDataResolver,
        customerData,
        setBillingAddressAction,
        globalMessageList,
        $t,
        shippingRatesValidator,
        registry
    ) {
        'use strict';

        var lastSelectedBillingAddress = null,
            countryData = customerData.get('directory-data'),
            addressOptions = addressList().filter(function (address) {
                return address.getType() === 'customer-address';
            });

        return Component.extend({
            defaults: {
                template: 'Magento_Checkout/billing-address',
                actionsTemplate: 'Magento_Checkout/billing-address/actions',
                formTemplate: 'Magento_Checkout/billing-address/form',
                detailsTemplate: 'Magento_Checkout/billing-address/details',
                links: {
                    isAddressFormVisible: '${$.billingAddressListProvider}:isNewAddressSelected'
                },
                imports: {
                    setApplyRule: "checkout.steps.billing-step.payment.payments-list.checkmo-form.form-fields.postcode:warn"
                }
            },
            currentBillingAddress: quote.billingAddress,
            street: ko.observable(''),
            customerHasAddresses: addressOptions.length > 0,
            collectBillingFlag: false,
            canApply: ko.observable(true),

            /**
             * Init component
             */
            initialize: function () {
                this._super();
                quote.paymentMethod.subscribe(function () {
                    checkoutDataResolver.resolveBillingAddress();
                }, this);
                shippingRatesValidator.initFields(this.get('name') + '.form-fields');
            },

            /**
             * @return {exports.initObservable}
             */
            initObservable: function () {
                this._super()
                    .observe({
                        selectedAddress: null,
                        isAddressDetailsVisible: quote.billingAddress() != null,
                        isAddressFormVisible: !customer.isLoggedIn() || !addressOptions.length,
                        isAddressSameAsShipping: false,
                        saveInAddressBook: 1
                    });
                quote.billingAddress.subscribe(function (newAddress) {
                    if (newAddress !== null && Array.isArray(newAddress.street)) {
                        this.street(newAddress.street.join(', '));
                    }

                    if (quote.isVirtual()) {
                        this.isAddressSameAsShipping(false);
                        //console.log(88);
                    } else if(quote.shippingMethod() && quote.shippingMethod().carrier_code == 'collect') {
                        /*BOC: 2j*/
                        this.isAddressSameAsShipping(false);
                        //console.log(90);
                        //console.log(quote.shippingMethod().method_code);
                        //console.log(quote.shippingMethod().carrier_code);
                    } else {
                        let tempVal = newAddress != null &&
                            newAddress.getCacheKey() == quote.shippingAddress().getCacheKey();
                        this.isAddressSameAsShipping(tempVal);
                        this.isAddressDetailsVisible(tempVal);
                    }

                    if(quote.shippingMethod() && typeof quote.shippingMethod() != 'undefined'){
                        if(quote.shippingMethod().carrier_code == 'amstorepickup'){
                            this.saveInAddressBook(0);
                            this.isAddressDetailsVisible(false);
                        }
                        else if(quote.shippingMethod().carrier_code != 'collect') {
                            if (newAddress != null && newAddress.saveInAddressBook !== undefined) {
                                this.saveInAddressBook(newAddress.saveInAddressBook);
                            } else {
                                this.saveInAddressBook(1);
                            }
                            this.isAddressDetailsVisible(true);
                        }
                        else {
                            /*BOC: 2j*/
                            this.saveInAddressBook(0);
                            this.isAddressDetailsVisible(this.collectBillingFlag);
                        }
                    }

                }, this);

                return this;
            },

            canUseShippingAddress: ko.computed(function () {
                /*BOC: 2j*/
                let method = quote.shippingMethod();
                if (typeof method != undefined && method && method.carrier_code === 'collect'){
                    return false;
                }
                if(!quote.isVirtual()){
                    if(quote.shippingAddress()){
                        if(quote.shippingAddress().canUseForBilling()){
                            if(typeof method != 'undefined' && method){
                                if('carrier_code' in method){
                                    if(method.carrier_code != 'collect' || method.carrier_code != 'amstorepickup'){
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                return false;
            }),

            canShowBillingAddressText: ko.computed(function () {
                if(typeof quote.shippingMethod() != 'undefined' && quote.shippingMethod()){
                    if('carrier_code' in quote.shippingMethod() ){
                        if(quote.shippingMethod().carrier_code == 'collect' || quote.shippingMethod().carrier_code == 'amstorepickup'){
                            return true
                        }
                    }
                }
                return false
            }),

            /**
             * @param {Object} address
             * @return {*}
             */
            addressOptionsText: function (address) {
                return address.getAddressInline();
            },

            /**
             * @return {Boolean}
             */
            useShippingAddress: function () {
                /*BOC: 2j*/
                if(quote.shippingMethod().carrier_code == 'collect') {
                    console.log(143);
                    lastSelectedBillingAddress = quote.billingAddress();
                    //quote.billingAddress(null);
                    $('.billing-address-same-as-shipping-block').hide();
                    this.isAddressDetailsVisible(true);
                    checkoutData.setSelectedBillingAddress(null);
                    return false;
                }
                /*EOC: 2j*/
                if (this.isAddressSameAsShipping()) {
                    selectBillingAddress(quote.shippingAddress());

                    this.updateAddresses();
                    this.isAddressDetailsVisible(true);
                } else {
                    lastSelectedBillingAddress = quote.billingAddress();
                    quote.billingAddress(null);
                    /**
                     * to prevent disabling the checkout button
                     */
                    this.isAddressDetailsVisible(false);
                }
                checkoutData.setSelectedBillingAddress(null);

                return true;
            },

            /**
             * Update address action
             */
            updateAddress: function () {
                var addressData, newBillingAddress;

                if (this.selectedAddress() && !this.isAddressFormVisible()) {
                    selectBillingAddress(this.selectedAddress());
                    checkoutData.setSelectedBillingAddress(this.selectedAddress().getKey());
                } else {
                    this.source.set('params.invalid', false);
                    this.source.trigger(this.dataScopePrefix + '.data.validate');

                    if (this.source.get(this.dataScopePrefix + '.custom_attributes')) {
                        this.source.trigger(this.dataScopePrefix + '.custom_attributes.data.validate');
                    }

                    if (!this.source.get('params.invalid')) {
                        addressData = this.source.get(this.dataScopePrefix);

                        if (customer.isLoggedIn() && !this.customerHasAddresses) { //eslint-disable-line max-depth
                            this.saveInAddressBook(1);
                        }
                        addressData['save_in_address_book'] = this.saveInAddressBook() ? 1 : 0;
                        newBillingAddress = createBillingAddress(addressData);

                        // New address must be selected as a billing address
                        selectBillingAddress(newBillingAddress);
                        checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                        checkoutData.setNewCustomerBillingAddress(addressData);

                        /*BOC: 2j*/
                        if (quote.shippingMethod().carrier_code == 'collect') {
                            this.collectBillingFlag = true;
                            this.isAddressDetailsVisible(true);
                        } else {
                            this.collectBillingFlag = false;
                            this.isAddressDetailsVisible(true);
                        }
                    }
                }
                this.updateAddresses();
            },

            /**
             * Edit address action
             */
            editAddress: function () {
                lastSelectedBillingAddress = quote.billingAddress();
                quote.billingAddress(null);
                this.isAddressDetailsVisible(false);
            },

            /**
             * Cancel address edit action
             */
            cancelAddressEdit: function () {
                this.restoreBillingAddress();

                if (quote.billingAddress()) {
                    // restore 'Same As Shipping' checkbox state
                    this.isAddressSameAsShipping(
                        quote.billingAddress() != null &&
                        quote.billingAddress().getCacheKey() == quote.shippingAddress().getCacheKey() && //eslint-disable-line
                        !quote.isVirtual()
                    );
                    this.isAddressDetailsVisible(true);
                }
            },

            /**
             * Manage cancel button visibility
             */
            canUseCancelBillingAddress: ko.computed(function () {
                return quote.billingAddress() || lastSelectedBillingAddress;
            }),

            /**
             * Restore billing address
             */
            restoreBillingAddress: function () {
                if (lastSelectedBillingAddress != null) {
                    selectBillingAddress(lastSelectedBillingAddress);
                }
            },

            /**
             * @param {Object} address
             */
            onAddressChange: function (address) {
                this.isAddressFormVisible(address == newAddressOption);
            },

            /**
             * @param {int} countryId
             * @return {*}
             */
            getCountryName: function (countryId) {
                return countryData()[countryId] != undefined ? countryData()[countryId].name : ''; //eslint-disable-line
            },

            /**
             * Trigger action to update shipping and billing addresses
             */
            updateAddresses: function () {
                if (window.checkoutConfig.reloadOnBillingAddress ||
                    !window.checkoutConfig.displayBillingOnPaymentMethod
                ) {
                    setBillingAddressAction(globalMessageList);
                }
            },

            /**
             * Get code
             * @param {Object} parent
             * @returns {String}
             */
            getCode: function (parent) {
                return _.isFunction(parent.getCode) ? parent.getCode() : 'shared';
            },

            setApplyRule: function (warn) {
                this.canApply(
                    !warn || warn.length === 0
                )
            },
        });
    });
