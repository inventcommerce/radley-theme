var config = {
    map: {
        '*': {
            "Magento_Checkout/js/view/billing-address": "Radley_BillingAddressStorePickup/js/view/checkout/view/billing-address",
            "Magento_Checkout/js/view/billing-address/list": "Radley_BillingAddressStorePickup/js/view/checkout/view/billing-address/list"
        }
    }
};
