define(function () {
    'use strict';

    var mixin = {
        opanAemWindow: function () {
            window.parent.postMessage({
                action: 'relative.redirect',
                target: "terms-conditions",
                content: this.content
            }, '*');
        },
        openAemPrivacyWindow: function () {
            window.parent.postMessage({
                action: 'relative.redirect',
                target: "privacy-cookie-policy",
                content: this.content
            }, '*');
        },
        privacyUrl: "/policy/privacy-cookie-policy"
    };

    return function (target) {
        return target.extend(mixin);
    };
});
